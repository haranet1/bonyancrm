/*---- placeholder1----*/
$(function() {
    'use strict'

    var sin = [],
        cos = [];
    for (var i = 0; i < 14; i += 0.1) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
    }
    var plot = $.plot("#placeholder1", [{
        data: sin
    }, {
        data: cos
    }], {
        series: {
            lines: {
                show: true
            }
        },
        lines: {
            show: true,
            fill: true,
            fillColor: {
                colors: [{
                    opacity: 0.7
                }, {
                    opacity: 0.7
                }]
            }
        },
        crosshair: {
            mode: "x"
        },
        grid: {
            hoverable: false,
            autoHighlight: false,
            borderColor: "rgba(119, 119, 142, 0.1)",
            verticalLines: false,
            horizontalLines: false
        },
        colors: ['#6c5ffc', '#05c3fb'],
        yaxis: {
            min: -1.2,
            max: 1.2,
            tickLength: 0
        },
        xaxis: {
            tickLength: 0
        }
    });

});

/*---- placeholder2----*/
$(function() {
    'use strict'

    var sin = [],
        cos = [];
    for (var i = 0; i < 14; i += 0.5) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
    }
    var plot = $.plot("#placeholder2", [{
        data: sin,
        label: "Ø¯ÛŒØªØ§1"
    }, {
        data: cos,
        label: "Ø¯ÛŒØªØ§2"
    }], {
        series: {
            lines: {
                show: true
            },
            points: {
                show: true
            }
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderColor: "rgba(119, 119, 142, 0.1)",
            verticalLines: false,
            horizontalLines: false
        },
        colors: ['#6c5ffc', '#05c3fb'],
        yaxis: {
            min: -1.2,
            max: 1.2,
            tickLength: 0
        },
        xaxis: {
            tickLength: 0
        }
    });
});


/*---- placeholder4----*/
$(function() {
    'use strict'

    // We use an inline data source in the example, usually data would
    // be fetched from a server
    var data = [],
        totalPoints = 300;

    function getRandomData() {
        'use strict'

        if (data.length > 0) data = data.slice(1);
        // Do a random walk
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }
            data.push(y);
        }
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }
        return res;
    }
    var updateInterval = 30;
    $("#updateInterval").val(updateInterval).change(function() {
        var v = $(this).val();
        if (v && !isNaN(+v)) {
            updateInterval = +v;
            if (updateInterval < 1) {
                updateInterval = 1;
            } else if (updateInterval > 2000) {
                updateInterval = 2000;
            }
            $(this).val("" + updateInterval);
        }
    });
    var plot = $.plot("#placeholder4", [getRandomData()], {
        series: {
            shadowSize: 0 // Drawing is faster without shadows
        },
        grid: {
            borderColor: "rgba(119, 119, 142, 0.1)",
        },
        colors: ["#6c5ffc"],
        yaxis: {
            min: 0,
            max: 100,
            tickLength: 0
        },
        xaxis: {
            tickLength: 0,
            show: false
        }
    });

    function update() {
        'use strict'
        plot.setData([getRandomData()]);
        plot.draw();
        setTimeout(update, updateInterval);
    }
    update();
});

/*---- placeholder6----*/
$(function() {
    'use strict'

    var d1 = [];
    for (var i = 0; i <= 10; i += 1) {
        d1.push([i, parseInt(Math.random() * 30)]);
    }
    var d2 = [];
    for (var i = 0; i <= 10; i += 1) {
        d2.push([i, parseInt(Math.random() * 30)]);
    }
    var d3 = [];
    for (var i = 0; i <= 10; i += 1) {
        d3.push([i, parseInt(Math.random() * 30)]);
    }
    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    function plotWithOptions() {
        'use strict'

        $.plot("#placeholder6", [d1, d2, d3], {
            series: {
                stack: stack,
                lines: {
                    show: lines,
                    fill: true,
                    steps: steps
                },
                bars: {
                    show: bars,
                    barWidth: 0.6
                }
            },
            grid: {
                borderColor: "rgba(119, 119, 142, 0.1)",
            },
            colors: ['#6c5ffc', '#05c3fb'],
            yaxis: {
                tickLength: 0
            },
            xaxis: {
                tickLength: 0,
                show: false
            }
        });
    }
    plotWithOptions();
    $(".stackControls button").click(function(e) {
        e.preventDefault();
        stack = $(this).text() == "Ø¨Ø§ Ø§Ù†Ø¨Ø§Ø´ØªÙ‡ Ø´Ø¯Ù†" ? true : null;
        plotWithOptions();
    });
    $(".graphControls button").click(function(e) {
        e.preventDefault();
        bars = $(this).text().indexOf("Ø³ØªÙˆÙ†") != -1;
        lines = $(this).text().indexOf("Ø®Ø·ÛŒ") != -1;
        steps = $(this).text().indexOf("Ù…Ø±Ø­Ù„Ù‡ Ø§ÛŒ") != -1;
        plotWithOptions();
    });
});

$(function() {
    'use strict'

    var data = [],
        series = Math.floor(Math.random() * 4) + 3;
    for (var i = 0; i < series; i++) {
        data[i] = {
            label: "Ø³Ø±ÛŒ " + (i + 1),
            data: Math.floor(Math.random() * 100) + 1
        }
    }
    var placeholder = $("#placeholder");
    $("#example-1").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ù†Ù…ÙˆØ¯Ø§Ø± Ø¯Ø§ÛŒØ±Ù‡ Ø§ÛŒ Ù¾ÛŒØ´ ÙØ±Ø¶");
        $("#description").text("Ù†Ù…ÙˆØ¯Ø§Ø± Ø¯Ø§ÛŒØ±Ù‡ Ø§ÛŒ Ù¾ÛŒØ´ ÙØ±Ø¶ Ø¨Ø¯ÙˆÙ† ØªÙ†Ø¸ÛŒÙ… Ú¯Ø²ÛŒÙ†Ù‡.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true
                }
            },
            colors: ['#6c5ffc', '#05c3fb', '#09ad95', '#1170e4', '#f82649'],
        });
    });
    $("#example-2").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ù¾ÛŒØ´ ÙØ±Ø¶ Ø¨Ø¯ÙˆÙ† Ø±Ø§Ù‡Ù†Ù…Ø§");
        $("#description").text("Ù†Ù…ÙˆØ¯Ø§Ø± Ø¯Ø§ÛŒØ±Ù‡ Ø§ÛŒ Ù¾ÛŒØ´ ÙØ±Ø¶ Ø²Ù…Ø§Ù†ÛŒ Ú©Ù‡ Ø§ÙØ³Ø§Ù†Ù‡ ØºÛŒØ±ÙØ¹Ø§Ù„ Ø§Ø³Øª. Ø§Ø² Ø¢Ù†Ø¬Ø§ÛŒÛŒ Ú©Ù‡ Ø¨Ø±Ú†Ø³Ø¨â€ŒÙ‡Ø§ Ù…Ø¹Ù…ÙˆÙ„Ø§Ù‹ Ø®Ø§Ø±Ø¬ Ø§Ø² Ø¸Ø±Ù Ù‡Ø³ØªÙ†Ø¯ØŒ Ø§Ù†Ø¯Ø§Ø²Ù‡ Ù†Ù…ÙˆØ¯Ø§Ø± Ø¨Ù‡ Ø§Ù†Ø¯Ø§Ø²Ù‡ Ù…Ù†Ø§Ø³Ø¨ ØªØºÛŒÛŒØ± Ù…ÛŒâ€ŒÚ©Ù†Ø¯.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true
                }
            },
            colors: ['#6c5ffc', '#05c3fb', '#09ad95', '#1170e4', '#f82649'],
            legend: {
                show: false
            }
        });
    });
    $("#example-3").on('click', function() {
        placeholder.unbind();
        $("#title").text("ÙØ±Ù…Øª Ú©Ù†Ù†Ø¯Ù‡ Ø¨Ø±Ú†Ø³Ø¨ Ø³ÙØ§Ø±Ø´ÛŒ");
        $("#description").text("ÛŒÚ© Ù¾Ø³ Ø²Ù…ÛŒÙ†Ù‡ Ù†ÛŒÙ…Ù‡ Ø´ÙØ§Ù Ø¨Ù‡ Ø¨Ø±Ú†Ø³Ø¨ Ù‡Ø§ Ùˆ ÛŒÚ© ØªØ§Ø¨Ø¹ labelFormatter Ø³ÙØ§Ø±Ø´ÛŒ Ø§Ø¶Ø§ÙÙ‡ Ø´Ø¯Ù‡ Ø§Ø³Øª.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    label: {
                        show: true,
                        radius: 1,
                        formatter: labelFormatter,
                        background: {
                            opacity: 0.8
                        }
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-4").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ø¨Ø±Ú†Ø³Ø¨ Ø´Ø¹Ø§Ø¹");
        $("#description").text("Ù¾Ø³â€ŒØ²Ù…ÛŒÙ†Ù‡â€ŒÙ‡Ø§ÛŒ Ø¨Ø±Ú†Ø³Ø¨ Ú©Ù…ÛŒ Ø´ÙØ§Ùâ€ŒØªØ± Ùˆ Ù…Ù‚Ø§Ø¯ÛŒØ± Ø´Ø¹Ø§Ø¹ Ø¨Ø±Ø§ÛŒ Ù‚Ø±Ø§Ø± Ø¯Ø§Ø¯Ù† Ø¢Ù†Ù‡Ø§ Ø¯Ø± Ù¾Ø§ÛŒ ØªÙ†Ø¸ÛŒÙ… Ø´Ø¯Ù‡ Ø§Ø³Øª.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    label: {
                        show: true,
                        radius: 3 / 4,
                        formatter: labelFormatter,
                        background: {
                            opacity: 0.5
                        }
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#64E572'],
            legend: {
                show: false
            }
        });
    });
    $("#example-5").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ø³Ø¨Ú© Ù‡Ø§ÛŒ Ø¨Ø±Ú†Ø³Ø¨ Ø´Ù…Ø§Ø±Ù‡ 1");
        $("#description").text("Ù¾Ø³ Ø²Ù…ÛŒÙ†Ù‡ Ù„ÛŒØ¨Ù„ Ù…Ø´Ú©ÛŒ Ø±Ù†Ú¯ Ù†ÛŒÙ…Ù‡ Ø´ÙØ§Ù.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    label: {
                        show: true,
                        radius: 3 / 4,
                        formatter: labelFormatter,
                        background: {
                            opacity: 0.5,
                            color: "#000"
                        }
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-6").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ø³Ø¨Ú© Ù‡Ø§ÛŒ Ø¨Ø±Ú†Ø³Ø¨ Ø´Ù…Ø§Ø±Ù‡ 2");
        $("#description").text("Ù¾Ø³ Ø²Ù…ÛŒÙ†Ù‡ Ù„ÛŒØ¨Ù„ Ø³ÛŒØ§Ù‡ Ø±Ù†Ú¯ Ù†ÛŒÙ…Ù‡ Ø´ÙØ§Ù Ø¯Ø± Ù„Ø¨Ù‡ Ù¾Ø§ÛŒ Ù‚Ø±Ø§Ø± Ø¯Ø§Ø¯Ù‡ Ø´Ø¯Ù‡ Ø§Ø³Øª.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 3 / 4,
                    label: {
                        show: true,
                        radius: 3 / 4,
                        formatter: labelFormatter,
                        background: {
                            opacity: 0.5,
                            color: "#000"
                        }
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-7").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ø¨Ø±Ú†Ø³Ø¨ Ù‡Ø§ÛŒ Ù¾Ù†Ù‡Ø§Ù†");
        $("#description").text("Ø§Ú¯Ø± Ø¨Ø±Ø´ Ú©Ù…ØªØ± Ø§Ø² Ø¯Ø±ØµØ¯ Ù…Ø¹ÛŒÙ†ÛŒ Ø§Ø² Ù¾Ø§ÛŒ (Ø¯Ø± Ø§ÛŒÙ† Ù…ÙˆØ±Ø¯ 10Ùª) Ø¨Ø§Ø´Ø¯ØŒ Ø¨Ø±Ú†Ø³Ø¨ Ù‡Ø§ Ù…ÛŒ ØªÙˆØ§Ù†Ù†Ø¯ Ù¾Ù†Ù‡Ø§Ù† Ø´ÙˆÙ†Ø¯.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    label: {
                        show: true,
                        radius: 2 / 3,
                        formatter: labelFormatter,
                        threshold: 0.1
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-8").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ø¨Ø±Ø´ ØªØ±Ú©ÛŒØ¨ÛŒ");
        $("#description").text("Ú†Ù†Ø¯ Ø¨Ø±Ø´ Ú©Ù…ØªØ± Ø§Ø² ÛŒÚ© Ø¯Ø±ØµØ¯ Ù…Ø¹ÛŒÙ† (Ø¯Ø± Ø§ÛŒÙ† Ù…ÙˆØ±Ø¯ 5Ùª) Ø§Ø² Ù¾Ø§ÛŒ Ø±Ø§ Ù…ÛŒ ØªÙˆØ§Ù† Ø¯Ø± ÛŒÚ© ØªÚ©Ù‡ Ø¨Ø²Ø±Ú¯ØªØ± ØªØ±Ú©ÛŒØ¨ Ú©Ø±Ø¯.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    combine: {
                        color: "#999",
                        threshold: 0.05
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-9").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ù¾Ø§ÛŒ Ù…Ø³ØªØ·ÛŒÙ„ÛŒ");
        $("#description").text("Ø´Ø¹Ø§Ø¹ Ø±Ø§ Ù…ÛŒ ØªÙˆØ§Ù† Ø±ÙˆÛŒ ÛŒÚ© Ø§Ù†Ø¯Ø§Ø²Ù‡ Ø®Ø§Øµ (Ø­ØªÛŒ Ø¨Ø²Ø±Ú¯ØªØ± Ø§Ø² Ø®ÙˆØ¯ Ø¸Ø±Ù) ØªÙ†Ø¸ÛŒÙ… Ú©Ø±Ø¯.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 500,
                    label: {
                        show: true,
                        formatter: labelFormatter,
                        threshold: 0.1
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-10").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ù¾Ø§ÛŒ Ú©Ø¬ Ø´Ø¯Ù‡");
        $("#description").text("Ù¾Ø§ÛŒ Ø±Ø§ Ù…ÛŒ ØªÙˆØ§Ù† Ø¯Ø± ÛŒÚ© Ø²Ø§ÙˆÛŒÙ‡ Ú©Ø¬ Ú©Ø±Ø¯.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    tilt: 0.5,
                    label: {
                        show: true,
                        radius: 1,
                        formatter: labelFormatter,
                        background: {
                            opacity: 0.8
                        }
                    },
                    combine: {
                        color: "#999",
                        threshold: 0.1
                    }
                }
            },
            colors: ['#1170e4', '#d43f8d', '#45aaf2', '#ecb403', '#e86a91'],
            legend: {
                show: false
            }
        });
    });
    $("#example-11").on('click', function() {
        placeholder.unbind();
        $("#title").text("Ø³ÙˆØ±Ø§Ø® Ø¯ÙˆÙ†Ø§Øª");
        $("#description").text("Ù…ÛŒ ØªÙˆØ§Ù† ÛŒÚ© Ø³ÙˆØ±Ø§Ø® Ø¯ÙˆÙ†Ø§Øª Ø§Ø¶Ø§ÙÙ‡ Ú©Ø±Ø¯.");
        $.plot(placeholder, data, {
            series: {
                pie: {
                    innerRadius: 0.5,
                    show: true
                }
            }
        });
    });
    $("#example-1").click();
});

// A custom label formatter used by several of the plots
function labelFormatter(label, series) {
    'use strict'
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
//
function setCode(lines) {
    'use strict'
    $("#code").text(lines.join("\n"));
}