<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('/v1')->group(function () {

    Route::post('/register', [ApiController::class, 'register']);

    Route::post('/getToken', [ApiController::class, 'getToken']);

    Route::prefix('user')->middleware('auth:sanctum')->group(function () {

        Route::post('/', function (Request $request) {
            return Auth::user();
        });

        Route::post('/deleteUser', function () {
            $del = Auth::user()->delete();
            if ($del)
                return response(['message' => 'user deleted']);
            return response(['message' => 'cant delete'], 400);
        });

        Route::post('/postProfile', [ApiController::class, 'updateProfile']);

        Route::post('/postPic', [ApiController::class, 'postProfilePic']);
        Route::post('/getPic', [ApiController::class, 'getProfilePic']);

        Route::post('/postTest', [ApiController::class, 'postTest']);

        Route::post('/createHabits', [ApiController::class, 'createHabits']);
        Route::post('/postHabit', [ApiController::class, 'postHabit']);
        Route::post('/deleteHabit', [ApiController::class, 'deleteHabit']);

        Route::post('/createHabitDays', [ApiController::class, 'createHabitDays']);
        Route::post('/postHabitDay', [ApiController::class, 'postHabitDay']);
        Route::post('/deleteHabitDay', [ApiController::class, 'deleteHabitDay']);

        Route::post('/postHabitHistory', [ApiController::class, 'postHistory']);

        Route::post('/postProject', [ApiController::class, 'postProject']);
        Route::post('/deleteProject', [ApiController::class, 'deleteProject']);

        Route::post('/postSubproject', [ApiController::class, 'postSubproject']);
        Route::post('/deleteSubproject', [ApiController::class, 'deleteSubproject']);

        Route::post('/postProgress', [ApiController::class, 'postProgress']);


//        getdata
        Route::post('/getAppVersion', [ApiController::class, 'getAppVersion']);

        Route::post('/getProfile', [ApiController::class, 'getProfile']);

        Route::post('/getTest', [ApiController::class, 'getTest']);

        Route::post('/getHabits', [ApiController::class, 'getHabits']);

        Route::post('/getHabitDays', [ApiController::class, 'getHabitDays']);

        Route::post('/getHabitHistories', [ApiController::class, 'getHistories']);

        Route::post('/getProjects', [ApiController::class, 'getProjects']);

        Route::post('/getSubprojects', [ApiController::class, 'getSubprojects']);

        Route::post('/getProgress', [ApiController::class, 'getProgress']);

    });

});

