<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('habit_days', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('app_id');
            $table->foreignId('habit_id');
            $table->dateTime('date');
            $table->integer('progress');
            $table->integer('max_progress');
            $table->integer('day');
            $table->boolean('expanded')->default(false);
            $table->boolean('completed')->default(false);
            $table->text('day_times');
            $table->text('day_unique_ids');
            $table->string('next_day_first_time');
            $table->boolean('history_created');
            $table->integer('cancelled_alarm_index');
            $table->float('day_score')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('habit_days');
    }
};
