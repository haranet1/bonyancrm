<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('family')->nullable();
            $table->boolean('sex')->nullable();
            $table->string('disc_type')->nullable();
            $table->date('birthday')->nullable();
            $table->text('province')->nullable();
            $table->text('city')->nullable();
            $table->string('job')->nullable();
            $table->boolean('is_married')->nullable();
            $table->string('mobile')->unique();
            $table->string('mobile_code')->nullable();
            $table->timestamp('mobile_code_at')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('active_plan')->nullable();
            $table->unsignedDouble('plan_expire_at')->nullable();
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('username')->nullable()->unique();
            $table->string('password')->nullable();
            $table->foreignId('photo_id')->nullable();
            $table->boolean('accepted')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
