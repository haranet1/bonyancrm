<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sub_projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->unsignedInteger('app_id');
            $table->foreignId('project_id');

            $table->string('title');
            $table->string('performer');
            $table->boolean('complete')->default(0);
            $table->string('notification_content');
            $table->string('target_index');

            $table->string('start_date');
            $table->string('end_date');

            $table->string('start_notification');
            $table->string('start_unique_alarm');
            $table->unsignedInteger('start_notification_hour');
            $table->unsignedInteger('start_notification_minute');

            $table->string('before_start_notification');
            $table->string('before_start_unique_alarm');
            $table->unsignedInteger('before_start_notification_hour');
            $table->unsignedInteger('before_start_notification_minute');

            $table->string('end_notification');
            $table->string('end_unique_alarm');
            $table->unsignedInteger('end_notification_hour');
            $table->unsignedInteger('end_notification_minute');

            $table->string('before_end_notification');
            $table->string('before_end_unique_alarm');
            $table->unsignedInteger('before_end_notification_hour');
            $table->unsignedInteger('before_end_notification_minute');

            $table->boolean('reminder_active')->default(false);
            $table->boolean('deleted')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sub_projects');
    }
};
