<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('habits', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->unsignedInteger('app_id');

            $table->string('name');
            $table->boolean('is_complete')->default(false);
            $table->unsignedInteger('done_counter')->default(0);
            $table->unsignedInteger('last_day');
            $table->string('skill_group');
            $table->string('icon_code')->nullable();
            $table->unsignedInteger('difficultly')->default(0);
            $table->unsignedInteger('importance')->default(0);
            $table->unsignedInteger('fear')->default(0);
            $table->unsignedInteger('coin')->default(0);
            $table->float('fail_ratio');
            $table->string('start_date');
            $table->string('end_date');
            $table->float('score');
            $table->string('hook');
            $table->integer('color');
            $table->string('week_days');
            $table->string('week_days_state');
            $table->integer('chosen_reminder');
            $table->boolean('is_active');
            $table->boolean('deleted')->default(false);
            $table->unsignedInteger('total_days');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('habits');
    }
};
