<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('progress', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->float('family_score');
            $table->float('health_score');
            $table->float('business_score');
            $table->float('communicate_score');
            $table->float('investment_score');
            $table->float('development_score');
            $table->float('lifestyle_score');
            $table->float('motivation_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('progress');
    }
};
