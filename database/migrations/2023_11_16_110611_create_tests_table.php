<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class,'user_id');
            $table->float('family_score')->nullable();
            $table->float('health_score')->nullable();
            $table->float('business_score')->nullable();
            $table->float('communicate_score')->nullable();
            $table->float('investment_score')->nullable();
            $table->float('development_score')->nullable();
            $table->float('lifestyle_score')->nullable();
            $table->float('motivation_score')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tests');
    }
};
