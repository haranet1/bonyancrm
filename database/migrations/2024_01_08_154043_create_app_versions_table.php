<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('app_versions', function (Blueprint $table) {
            $table->id();
            $table->unsignedFloat('version')->default(1);
            $table->boolean('required')->default(false);
            $table->string('name')->nullable();
            $table->text('change_log')->nullable();
            $table->dateTime('update_at')->default(\Carbon\Carbon::now())->nullable();
            $table->text('direct_download')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('app_versions');
    }
};
