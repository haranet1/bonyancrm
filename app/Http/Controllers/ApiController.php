<?php

namespace App\Http\Controllers;

use App\Models\AppVersion;
use App\Models\Habit;
use App\Models\HabitDay;
use App\Models\HabitHistory;
use App\Models\Image;
use App\Models\Progress;
use App\Models\Project;
use App\Models\SubProject;
use App\Models\Test;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    //

    public function register(Request $request)
    {
        $validator = validator($request->all(), [
            'mobile' => 'required|string|size:11',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }


        $user = User::where('mobile', $request->mobile)->first();
        if ($user) {
            if ($user->mobile_code_at && Carbon::create($user->mobile_code_at)->diffInMinutes(Carbon::now()) < 1)
                return ['message' => 'sms cool down'];
        }

        $code = rand(1000, 9999);
        $response = Http::withHeaders(['Accept' => 'application/json'])->
        post('https://console.melipayamak.com/api/send/shared/0489a98d564c4275900806aba9a01881', [
            'bodyId' => 194153,
            'to' => '' . $request->mobile,
            'args' => ['' . $code],
        ]);
        $response = json_decode($response);
        if ($response->status && $response->status == 'ارسال موفق بود') {

            $existUser = User::where('mobile',$request->mobile)->first();
            if ($existUser){
                $existUser->update([
                    'mobile_code_at' => Carbon::now(),
                    'mobile_verified_at' => null,
                    'mobile_code' => $code,
                ]);
                return response()->json(['message' => 'user exist'], 200);
            }else{
                User::create([
                    'mobile' => $request->mobile,
                    'name' => $request->name,
                    'family' => $request->family,
                    'sex' => $request->sex,
                    'mobile_code_at' => Carbon::now(),
                    'mobile_verified_at' => null,
                    'mobile_code' => $code,
                ]);
                return response()->json(['message' => 'register done'], 200);
            }

        } else
            return response()->json(['message' => 'sms failed'], 500);
    }

    public function getToken(Request $request)
    {
        $validator = validator($request->all(), [
            'mobile' => 'required|string|size:11',
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $user = User::where('mobile', $request->mobile)->where('mobile_code', $request->code)
            ->whereBetween('mobile_code_at', [Carbon::now()->subMinute(5), Carbon::now()])->whereNull('mobile_verified_at')->first();
        if ($user) {
            $user->tokens()->delete();
            $token = $user->createToken($request->mobile);
            $user->update(['mobile_verified_at' => Carbon::now()]);
            return response()->json(['message' => 'ok', 'token' => $token->plainTextToken], 200);
        }
        return response()->json(['message' => 'mobile or code is invalid'], 500);
    }


    public function updateProfile(Request $request)
    {
        $validator = validator($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'job' => 'required',
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $update = Auth::user()->update([
            'name' => $request->name,
            'family' => $request->last_name,
            'birthday' => Carbon::createFromTimestamp($request->birthday / 1000)->toDateTime(),
            'job' => $request->job,
            'email' => $request->email,
            'sex' => $request->sex,
            'province' => $request->province,
            'city' => $request->city,
            'is_married' => $request->is_married,
            'active_plan' => $request->active_plan,
            'plan_expire_at' => $request->plan_expire_at,
        ]);
        if ($update)
            return response()->json(['message' => 'profile saved'], 200);
        return response()->json(['message' => 'profile failed'], 500);
    }

    public function postProfilePic(Request $request)
    {
        // بررسی وجود فایل عکس
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            // ذخیره عکس در مسیر مورد نظر (مثلاً public/images)
            $image->move(public_path('uploads/images/profile pictures/'), $imageName);
            $imageModel = new Image();
            $imageModel->path = 'uploads/images/profile pictures/' . $imageName; // مسیر عکس در دیتابیس
            $imageModel->user_id =Auth::id();
            $imageModel->save();
            // ارسال پیام به کلاینت
            return response()->json(['message' => 'عکس با موفقیت ذخیره شد'], 200);
        } else {
            // در صورت عدم وجود عکس، ارسال پیام خطا به کلاینت
            return response()->json(['error' => 'هیچ عکسی انتخاب نشده است'], 400);
        }
    }

    public function getProfilePic()
    {
        $image = Image::where('user_id',Auth::id())->orderByDesc('created_at')->first();
        if (!$image) {
            return response()->json(['error' => 'تصویر مورد نظر یافت نشد'], 404);
        }

        $filePath = public_path( $image->path);
        if (!file_exists($filePath)) {
            return response()->json(['error' => 'فایل تصویر یافت نشد'], 404);
        }

        return response()->download($filePath, 'profilePicture');
    }

    public function getAppVersion()
    {
        return response()->json(AppVersion::latest()->first());
    }

    public function getProfile()
    {
        return User::where('id',Auth::user()->id)->with('progress','test')->get();
    }

    public function postTest(Request $request)
    {
        $validator = validator($request->all(), [
            'family_score' => 'required',
            'health_score' => 'required',
            'business_score' => 'required',
            'communicate_score' => 'required',
            'investment_score' => 'required',
            'development_score' => 'required',
            'lifestyle_score' => 'required',
            'motivation_score' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $create = Test::updateOrCreate([
            'user_id' => Auth::user()->id,
        ], [
            'family_score' => $request->family_score,
            'health_score' => $request->health_score,
            'business_score' => $request->business_score,
            'communicate_score' => $request->communicate_score,
            'investment_score' => $request->investment_score,
            'development_score' => $request->development_score,
            'lifestyle_score' => $request->lifestyle_score,
            'motivation_score' => $request->motivation_score,
        ]);
        if ($create)
            return response()->json(['message' => 'test saved'], 200);
        return response()->json(['message' => 'test failed'], 500);
    }

    public function getTest()
    {
        if (Auth::user()->test()->exists())
            return response(['message' => 'ok', 'data' => Auth::user()->test()]);
        return response(['message' => 'test not found'], 200);
    }


    public function postProgress(Request $request)
    {
        $validator = validator($request->all(), [
            'family_score' => 'required',
            'health_score' => 'required',
            'business_score' => 'required',
            'communicate_score' => 'required',
            'investment_score' => 'required',
            'development_score' => 'required',
            'lifestyle_score' => 'required',
            'motivation_score' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $pro = Progress::updateOrCreate([
            'user_id' => Auth::user()->id,
        ], [
            'family_score' => $request->family_score,
            'health_score' => $request->health_score,
            'business_score' => $request->business_score,
            'communicate_score' => $request->communicate_score,
            'investment_score' => $request->investment_score,
            'development_score' => $request->development_score,
            'lifestyle_score' => $request->lifestyle_score,
            'motivation_score' => $request->motivation_score,
        ]);
        if ($pro)
            return response()->json(['message' => 'progress saved'], 200);
        return response()->json(['message' => 'progress failed'], 500);
    }

    public function getProgress()
    {
        if (Auth::user()->progress()->exists())
            return response(['message' => 'ok', 'data' => Auth::user()->progress()]);
        return response(['message' => 'progress not found'], 200);
    }


    public function createHabits(Request $request)
    {
        $validator = validator($request->all(), [
            'habits' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $data = [];
        foreach ($request->habits as $habit) {
            array_push($data, [
                'user_id' => Auth::user()->id,
                'app_id' => $habit->id,
                'name' => $habit->name,
                'is_complete' => $habit->is_complete,
                'done_counter' => $habit->done_counter,
                'last_day' => $habit->last_day,
                'skill_group' => $habit->skill_group,
                'icon_code' => $habit->icon_code,
                'difficultly' => $habit->difficultly,
                'importance' => $habit->importance,
                'fear' => $habit->fear,
                'coin' => $habit->coin,
                'fail_ratio' => $habit->fail_ratio,
                'start_date' => $habit->start_date,
                'end_date' => $habit->end_date,
                'score' => $habit->score,
                'hook' => $habit->hook,
                'color' => $habit->color,
                'week_days' => $habit->week_days,
                'week_days_state' => $habit->week_days_state,
                'chosen_reminder' => $habit->chosen_reminder,
                'is_active' => $habit->is_active,
                'total_days' => $habit->total_days,
            ]);
        }
        $habits = Habit::insert($data);
        if ($habits)
            return response()->json(['message' => 'habits saved'], 200);
        return response()->json(['message' => 'habits failed'], 500);
    }

    public function postHabit(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'is_complete' => 'required',
            'done_counter' => 'required',
            'last_day' => 'required',
            'skill_group' => 'required',
            'icon_code' => 'required',
            'difficultly' => 'required',
            'importance' => 'required',
            'fear' => 'required',
            'coin' => 'required',
            'fail_ratio' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'score' => 'required',
            'hook' => 'required',
            'color' => 'required',
            'week_days' => 'required',
            'week_days_state' => 'required',
            'chosen_reminder' => 'required',
            'is_active' => 'required',
            'total_days' => 'required',
            'deleted' => 'required',
            'is_edit' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        try {
            $habit = Habit::updateOrCreate([
                'user_id' => Auth::user()->id,
                'app_id' => $request->id,
            ], [
                'name' => $request->name,
                'is_complete' => $request->is_complete,
                'done_counter' => $request->done_counter,
                'last_day' => $request->last_day,
                'skill_group' => $request->skill_group,
                'icon_code' => json_encode($request->icon_code),
                'difficultly' => $request->difficultly,
                'importance' => $request->importance,
                'fear' => $request->fear,
                'coin' => $request->coin,
                'fail_ratio' => $request->fail_ratio,
                'start_date' => "" . $request->start_date,
                'end_date' => "" . $request->end_date,
                'score' => $request->score,
                'hook' => $request->hook,
                'color' => $request->color,
                'week_days' => $request->week_days,
                'week_days_state' => $request->week_days_state,
                'chosen_reminder' => $request->chosen_reminder,
                'is_active' => $request->is_active,
                'total_days' => $request->total_days,
                'deleted' => $request->deleted,
            ]);

            if ($habit->wasRecentlyCreated) {
                return response()->json(['message' => 'habit created'], 200);
            } else {
                if($request->is_edit){
                    HabitDay::where('user_id', Auth::user()->id)->where('habit_id', $habit->app_id)->whereDate('date', '>', Carbon::yesterday()->addDay())->delete();
                    return response()->json(['message' => 'older habit days deleted & habit edited'], 200);
                }
                return response()->json(['message' => 'habit updated'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }


    }

    public function getHabits()
    {
        $habits = Habit::where('user_id', Auth::id())->with('habitDays', 'histories')->get();
        if ($habits)
            return response(['message' => 'ok', 'data' => $habits]);
        return response(['message' => 'habits not found'], 200);
    }

    public function deleteHabit(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required|int',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $habit = Habit::where('user_id', Auth::user()->id)->where('app_id', $request->id)->first();
        if ($habit) {
            $habit->deleted = true;
            $habit->save();

            $deletedRows = HabitDay::where('user_id', Auth::user()->id)->where('habit_id', $habit->id)->whereDate('date', '>', Carbon::now())->delete();
            if ($deletedRows > 0)
                return response()->json(['message' => 'habit deleted & ' . $deletedRows . ' additional habit days deleted'], 200);

            return response()->json(['message' => 'habit soft deleted'], 200);
        }
        return response()->json(['message' => 'habit not found'], 404);
    }


    public function createHabitDays(Request $request)
    {
        $validator = validator($request->all(), [
            'habitDays' => 'required|array',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $data = [];
        foreach ($request->habitDays as $habitDay) {
            array_push($data, [
                'user_id' => Auth::user()->id,
                'app_id' => $habitDay->id,
                'habit_id' => $habitDay->habit_id,
                'progress' => $habitDay->progress,
                'max_progress' => $habitDay->max_progress,
                'day' => $habitDay->day,
                'date' => $habitDay->date,
                'expanded' => $habitDay->expanded,
                'completed' => $habitDay->completed,
                'day_times' => $habitDay->day_times,
                'day_unique_ids' => $habitDay->day_unique_ids,
                'next_day_first_time' => $habitDay->next_day_first_time,
                'history_created' => $habitDay->history_created,
                'cancelled_alarm_index' => $habitDay->cancelled_alarm_index,
                'day_score' => $habitDay->day_score,
            ]);
        }
        $habitDays = HabitDay::insert($data);
        if ($habitDays)
            return response()->json(['message' => 'habitDays saved'], 200);
        return response()->json(['message' => 'habitDays failed'], 500);
    }

    public function postHabitDay(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required|int',
            'habit_id' => 'required|int',
            'progress' => 'required',
            'max_progress' => 'required',
            'day' => 'required',
            'date' => 'required',
            'expanded' => 'required',
            'completed' => 'required',
            'day_times' => 'required',
            'day_unique_ids' => 'required',
            'next_day_first_time' => 'required',
            'history_created' => 'required',
            'cancelled_alarm_index' => 'required',
            'day_score' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        try {
            $habitDay = HabitDay::updateOrCreate([
                'user_id' => Auth::user()->id,
                'app_id' => $request->id,
                'habit_id' => $request->habit_id,
            ], [
                'progress' => $request->progress,
                'max_progress' => $request->max_progress,
                'day' => $request->day,
                'date' => Carbon::createFromTimestamp($request->date / 1000)->toDateTime(),
                'expanded' => $request->expanded,
                'completed' => $request->completed,
                'day_times' => $request->day_times,
                'day_unique_ids' => $request->day_unique_ids,
                'next_day_first_time' => $request->next_day_first_time,
                'history_created' => $request->history_created,
                'cancelled_alarm_index' => $request->cancelled_alarm_index,
                'day_score' => $request->day_score,
            ]);
            if ($habitDay->wasRecentlyCreated) {
                return response()->json(['message' => 'HabitDay created'], 200);
            } else {
                return response()->json(['message' => 'HabitDay updated'], 200);
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function getHabitDays()
    {
        if (Auth::user()->habitDays()->exists())
            return response(['message' => 'ok', 'data' => Auth::user()->habitDays()]);
        return response(['message' => 'habitDays not found'], 200);
    }

    public function deleteHabitDay(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $habitDay = HabitDay::where('user_id', Auth::user()->id)->where('app_id', $request->id)->first();
        if ($habitDay) {
            $habitDay->delete();
            return response()->json(['message' => 'habitDay deleted'], 200);
        }
        return response()->json(['message' => 'habitDay not found'], 404);
    }


    public function postHistory(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
            'habit_id' => 'required',
            'habit_day_id' => 'required',
            'set_time' => 'required',
            'day' => 'required',
            'step_day' => 'required',
            'step_done' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        try {
            $history = HabitHistory::updateOrCreate([
                'user_id' => Auth::user()->id,
                'app_id' => $request->id,
                'habit_id' => $request->habit_id,
                'habit_day_id' => $request->habit_day_id,
            ], [
                'date' => $request->set_time,
                'day' => $request->day,
                'step_day' => $request->step_day,
                'step_done' => $request->step_done,
            ]);
            if ($history->wasRecentlyCreated) {
                return response()->json(['message' => 'History created'], 200);
            } else {
                return response()->json(['message' => 'History updated'], 200);
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function getHistories(Request $request)
    {
        $hhs = HabitHistory::where('habit_id', $request->habit_id)->where('habit_day_id', $request->habit_day_id)->get();
        if (hhs)
            return response(['message' => 'ok', 'data' => $hhs]);
        return response(['message' => 'habit histories not found'], 200);
    }


    public function postProject(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'progress' => 'required',
            'is_complete' => 'required',
            //    'notification_content' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_notification' => 'required',
            'start_unique_alarm' => 'required',
            'start_notification_hour' => 'required',
            'start_notification_minute' => 'required',
            'before_start_notification' => 'required',
            'before_start_unique_alarm' => 'required',
            'before_start_notification_hour' => 'required',
            'before_start_notification_minute' => 'required',
            'end_notification' => 'required',
            'end_unique_alarm' => 'required',
            'end_notification_hour' => 'required',
            'end_notification_minute' => 'required',
            'before_end_notification' => 'required',
            'before_end_unique_alarm' => 'required',
            'before_end_notification_hour' => 'required',
            'before_end_notification_minute' => 'required',
            's_count' => 'required',
            'reminder_active' => 'required',
            'deleted' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        try {
            $project = Project::updateOrCreate([
                'user_id' => Auth::user()->id,
                'app_id' => $request->id,
            ], [
                'title' => $request->title,
                'description' => $request->description,
                'progress' => $request->progress,
                'complete' => $request->is_complete,
                'notification_content' => $request->notification_content,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'start_notification' => $request->start_notification,
                'start_unique_alarm' => $request->start_unique_alarm,
                'start_notification_hour' => $request->start_notification_hour,
                'start_notification_minute' => $request->start_notification_minute,
                'before_start_notification' => $request->before_start_notification,
                'before_start_unique_alarm' => $request->before_start_unique_alarm,
                'before_start_notification_hour' => $request->before_start_notification_hour,
                'before_start_notification_minute' => $request->before_start_notification_minute,
                'end_notification' => $request->end_notification,
                'end_unique_alarm' => $request->end_unique_alarm,
                'end_notification_hour' => $request->end_notification_hour,
                'end_notification_minute' => $request->end_notification_minute,
                'before_end_notification' => $request->before_end_notification,
                'before_end_unique_alarm' => $request->before_end_unique_alarm,
                'before_end_notification_hour' => $request->before_end_notification_hour,
                'before_end_notification_minute' => $request->before_end_notification_minute,
                's_count' => $request->s_count,
                'reminder_active' => $request->reminder_active,
                'deleted' => $request->deleted,
            ]);

            if ($request->deleted){
                $project->updateSubProjectsColumn('deleted',true);
                return response()->json(['message' => 'project & subs soft deleted'], 200);
            }

            if ($project->wasRecentlyCreated) {
                return response()->json(['message' => 'project created'], 200);
            } else {
                return response()->json(['message' => 'project updated'], 200);
            }
        } catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function deleteProject(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $project = Project::where('user_id', Auth::user()->id)->where('app_id', $request->id)->with('subProjects')->first();
        if ($project) {
            if ($project->subProjects()->exists()) {
                $project->subProjects()->delete();
            }
            $project->delete();
            return response()->json(['message' => 'project deleted'], 200);
        }
        return response()->json(['message' => 'project not found'], 404);
    }

    public function getProjects(Request $request)
    {
        $pros = Project::where('user_id', Auth::id())->get();
        if ($pros)
            return response(['message' => 'ok', 'data' => $pros]);
        return response(['message' => 'projects not found'], 404);
    }

    public function getSubprojects(Request $request)
    {
        $subs = SubProject::where('user_id', Auth::id())->get();
        if ($subs)
            return response(['message' => 'ok', 'data' => $subs]);
        return response(['message' => 'subProjects not found'], 404);
    }


    public function postSubproject(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
            'project_id' => 'required',
            'title' => 'required',
            'performer' => 'required',
            'complete' => 'required',
            'notification_content' => 'required',
            'target_index' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_notification' => 'required',
            'start_unique_alarm' => 'required',
            'start_notification_hour' => 'required',
            'start_notification_minute' => 'required',
            'before_start_notification' => 'required',
            'before_start_unique_alarm' => 'required',
            'before_start_notification_hour' => 'required',
            'before_start_notification_minute' => 'required',
            'end_notification' => 'required',
            'end_unique_alarm' => 'required',
            'end_notification_hour' => 'required',
            'end_notification_minute' => 'required',
            'before_end_notification' => 'required',
            'before_end_unique_alarm' => 'required',
            'before_end_notification_hour' => 'required',
            'before_end_notification_minute' => 'required',
            'reminder_active' => 'required',
            'deleted' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        try {
            $sub = SubProject::updateOrCreate([
                'user_id' => Auth::user()->id,
                'app_id' => $request->id,
                'project_id' => $request->project_id,
            ], [
                'title' => $request->title,
                'performer' => $request->performer,
                'complete' => $request->complete,
                'notification_content' => $request->notification_content,
                'target_index' => $request->target_index,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'start_notification' => $request->start_notification,
                'start_unique_alarm' => $request->start_unique_alarm,
                'start_notification_hour' => $request->start_notification_hour,
                'start_notification_minute' => $request->start_notification_minute,
                'before_start_notification' => $request->before_start_notification,
                'before_start_unique_alarm' => $request->before_start_unique_alarm,
                'before_start_notification_hour' => $request->before_start_notification_hour,
                'before_start_notification_minute' => $request->before_start_notification_minute,
                'end_notification' => $request->end_notification,
                'end_unique_alarm' => $request->end_unique_alarm,
                'end_notification_hour' => $request->end_notification_hour,
                'end_notification_minute' => $request->end_notification_minute,
                'before_end_notification' => $request->before_end_notification,
                'before_end_unique_alarm' => $request->before_end_unique_alarm,
                'before_end_notification_hour' => $request->before_end_notification_hour,
                'before_end_notification_minute' => $request->before_end_notification_minute,
                'reminder_active' => $request->reminder_active,
                'deleted' => $request->deleted,
            ]);
            if ($sub->wasRecentlyCreated) {
                return response()->json(['message' => 'subProject created'], 200);
            } else {
                return response()->json(['message' => 'subProject updated'], 200);
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function deleteSubproject(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $sub = SubProject::where('user_id', Auth::user()->id)->where('app_id', $request->id)->first();
        if ($sub) {
            $sub->delete();
            return response()->json(['message' => 'subproject deleted'], 200);
        }
        return response()->json(['message' => 'subproject not found'], 404);
    }
}
