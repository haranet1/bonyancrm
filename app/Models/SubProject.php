<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubProject extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'app_id',
        'project_id',
        'title',
        'performer',
        'complete',
        'notification_content',
        'target_index',
        'start_date',
        'end_date',
        'start_notification',
        'start_unique_alarm',
        'start_notification_hour',
        'start_notification_minute',
        'before_start_notification',
        'before_start_unique_alarm',
        'before_start_notification_hour',
        'before_start_notification_minute',
        'end_notification',
        'end_unique_alarm',
        'end_notification_hour',
        'end_notification_minute',
        'before_end_notification',
        'before_end_unique_alarm',
        'before_end_notification_hour',
        'before_end_notification_minute',
        'reminder_active',
        'deleted',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id','user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'app_id');
    }
}
