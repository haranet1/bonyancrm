<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HabitDay extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'app_id',
        'habit_id',
        'progress',
        'max_progress',
        'day',
        'date',
        'expanded',
        'completed',
        'day_times',
        'day_unique_ids',
        'next_day_first_time',
        'history_created',
        'cancelled_alarm_index',
        'day_score',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id','user_id');
    }

    public function habit(){
        return $this->belongsTo(Habit::class,'app_id','habit_id');
    }

    public function history(){
        return $this->hasOne(HabitHistory::class,'habit_day_id','app_id');
    }
}
