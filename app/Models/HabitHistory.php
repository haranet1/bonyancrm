<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HabitHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'habit_day_id',
        'app_id',
        'habit_id',
        'date',
        'day',
        'step_day',
        'step_done',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id','user_id');
    }

    public function habit(){
        return $this->belongsTo(Habit::class,'app_id','habit_id');
    }

    public function habitDay(){
        return $this->belongsTo(HabitDay::class,'app_id','habit_day_id');
    }
}
