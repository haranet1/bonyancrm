<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'family',
        'mobile',
        'mobile_code',
        'mobile_verified_at',
        'mobile_code_at',
        'sex',
        'disc_type',
        'birthday',
        'province',
        'city',
        'username',
        'email',
        'password',
        'job',
        'is_married',
        'active_plan',
        'plan_expire_at',
        'email_verified_at',
        'accepted',
        'photo_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'mobile_code',
        'mobile_code_at',
        'mobile_verified_at',
        'email_verified_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function habits(){
        return $this->hasMany(Habit::class,'user_id','id');
    }

    public function habitDays(){
        return $this->hasMany(HabitDay::class,'user_id','id');
    }

    public function habitHistories(){
        return $this->hasMany(HabitHistory::class,'user_id','id');
    }

    public function test(){
        return $this->hasOne(Test::class,'user_id','id');
    }

    public function progress(){
        return $this->hasOne(Progress::class,'user_id','id');
    }

    public function projects(){
        return $this->hasMany(Project::class,'user_id','id');
    }

    public function subProjects(){
        return $this->hasMany(SubProject::class,'user_id','id');
    }

}
