<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'app_id',
        'title',
        'description',
        'progress',
        'complete',
        'notification_content',
        'start_date',
        'end_date',
        'start_notification',
        'start_unique_alarm',
        'start_notification_hour',
        'start_notification_minute',
        'before_start_notification',
        'before_start_unique_alarm',
        'before_start_notification_hour',
        'before_start_notification_minute',
        'end_notification',
        'end_unique_alarm',
        'end_notification_hour',
        'end_notification_minute',
        'before_end_notification',
        'before_end_unique_alarm',
        'before_end_notification_hour',
        'before_end_notification_minute',
        's_count',
        'reminder_active',
        'deleted',
    ];

    public function subProjects(){
        return $this->hasMany(SubProject::class,'project_id','app_id');
    }

    public function updateSubProjectsColumn($columnName, $value)
    {
        $this->subProjects()->update([$columnName => $value]);
    }

    public function user(){
        return $this->belongsTo(User::class,'id','user_id');
    }
}
