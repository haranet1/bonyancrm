<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Habit extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'app_id',
        'name',
        'is_complete',
        'done_counter',
        'last_day',
        'skill_group',
        'icon_code',
        'difficultly',
        'importance',
        'fear',
        'coin',
        'fail_ratio',
        'start_date',
        'end_date',
        'score',
        'hook',
        'color',
        'week_days',
        'week_days_state',
        'chosen_reminder',
        'is_active',
        'total_days',
        'deleted',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id','user_id');
    }

    public function habitDays(){
        return $this->hasMany(HabitDay::class,'user_id','user_id');
    }

    public function histories(){
        return $this->hasMany(HabitHistory::class,'user_id','user_id');
    }
}
