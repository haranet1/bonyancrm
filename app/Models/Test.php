<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'family_score',
        'health_score',
        'business_score',
        'communicate_score',
        'investment_score',
        'development_score',
        'lifestyle_score',
        'motivation_score',
    ];

    public function user(){
        return $this->belongsTo(User::class,'id','user_id');
    }
}
