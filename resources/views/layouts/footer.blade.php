<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-md-12 col-sm-12 text-center">
                حق نشر © 1402 <a href="https://bonyangozar.ir" target="_blank">بنیانگذار</a>. طراحی شده با <span class="fa fa-heart text-danger"></span> توسط <a href="https://haranet.ir" target="_blank"> حرانت </a>
                کلیه حقوق محفوظ است.
            </div>
        </div>
    </div>
</footer>