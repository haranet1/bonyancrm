<div class="sticky">
    <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
    <div class="app-sidebar">
        <div class="side-header">
            <a class="header-brand1" href="index.html">
                <img src="{{ asset('assets/images/brand/logo.png') }}" class="header-brand-img desktop-logo"
                     alt="logo">
                <img src="{{ asset('assets/images/brand/logo-1.png') }}" class="header-brand-img toggle-logo"
                     alt="logo">
                <img src="{{ asset('assets/images/brand/logo-2.png') }}" class="header-brand-img light-logo"
                     alt="logo">
                <img src="{{ asset('assets/images/brand/logo-3.png') }}" class="header-brand-img light-logo1"
                     alt="logo">
            </a>

        </div>
        <div class="main-sidemenu">
            <div class="slide-left disabled" id="slide-left">
                <svg xmlns="http://www.w3.org/2000/svg"
                     fill="#7b8191" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"/>
                </svg>
            </div>
            <ul class="side-menu">

                <li class="sub-category">
                    <h3>اصلی</h3>
                </li>
                <li class="slide">
                    <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{route('dashboard')}}"><i
                            class="side-menu__icon fe fe-home"></i><span
                            class="side-menu__label">داشبورد</span></a>
                </li>

                <li class="sub-category">
                    <h3>مدیریت</h3>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fa fa-diamond"></i>
                        <span class="side-menu__label">امکانات</span>
                        <i class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">امکانات</a></li>
                        <li><a href="calendar2.html" class="slide-item">آمار اپلیکیشن</a></li>
                        <li><a href="calendar.html" class="slide-item">مدیریت پشتیبانان</a></li>
                        <li><a href="" class="slide-item">مدیریت مشترکین</a></li>
                        <li><a href="chat.html" class="slide-item">ارسال پیام گروهی</a></li>
                    </ul>
                </li>

                <li class="sub-category">
                    <h3> کیت UI</h3>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-slack"></i><span class="side-menu__label">برنامه
                            ها</span><i class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">برنامه‌ها</a></li>
                        <li><a href="cards.html" class="slide-item"> کارت</a></li>
                        <li><a href="calendar.html" class="slide-item"> تقویم پیش‌فرض</a></li>
                        <li><a href="calendar2.html" class="slide-item"> تقویم کامل</a></li>
                        <li><a href="chat.html" class="slide-item"> گپ</a></li>
                        <li><a href="notify.html" class="slide-item"> اعلان‌ها</a></li>
                        <li><a href="sweetalert.html" class="slide-item"> سویت آلرت</a></li>
                        <li><a href="rangeslider.html" class="slide-item"> نوار بازه محدوده</a></li>
                        <li><a href="scroll.html" class="slide-item"> نوار پیمایش محتوا</a></li>
                        <li><a href="loaders.html" class="slide-item"> لودرها</a></li>
                        <li><a href="counters.html" class="slide-item"> شمارنده</a></li>
                        <li><a href="rating.html" class="slide-item"> رتبه‌بندی</a></li>
                        <li><a href="timeline.html" class="slide-item"> تایم لاین</a></li>
                        <li><a href="treeview.html" class="slide-item"> نمای درختی</a></li>
                        <li><a href="chart.html" class="slide-item"> نمودارها</a></li>
                        <li><a href="footers.html" class="slide-item"> پاورقی</a></li>
                        <li><a href="users-list.html" class="slide-item"> فهرست کاربران</a></li>
                        <li><a href="search.html" class="slide-item">جستجو</a></li>
                        <li><a href="crypto-currencies.html" class="slide-item"> ارزهای رمزنگلری شده</a>
                        </li>
                        <li><a href="widgets.html" class="slide-item"> ابزارک‌ها</a></li>
                    </ul>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-package"></i><span class="side-menu__label">بوت
                            استرپ</span><i class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu mega-slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">بوت استرپ</a></li>
                        <div class="mega-menu">
                            <div class="">
                                <ul>
                                    <li><a href="alerts.html" class="slide-item"> هشدارها</a></li>
                                    <li><a href="buttons.html" class="slide-item"> دکمه‌ها</a></li>
                                    <li><a href="colors.html" class="slide-item"> رنگ‌ها</a></li>
                                    <li><a href="avatarsquare.html" class="slide-item"> آواتار مربعی</a>
                                    </li>
                                    <li><a href="avatar-radius.html" class="slide-item"> آواتار گرد</a>
                                    </li>
                                    <li><a href="avatar-round.html" class="slide-item"> آواتار دایره
                                            ای</a></li>
                                    <li><a href="dropdown.html" class="slide-item"> دراپ داون</a></li>
                                </ul>
                            </div>
                            <div class="">
                                <ul>
                                    <li><a href="listgroup.html" class="slide-item"> گروه فهرست</a></li>
                                    <li><a href="tags.html" class="slide-item"> برچسب‌ها</a></li>
                                    <li><a href="pagination.html" class="slide-item"> صفحه بندی</a></li>
                                    <li><a href="navigation.html" class="slide-item"> پیمایش</a></li>
                                    <li><a href="typography.html" class="slide-item"> تایپوگرافی</a></li>
                                    <li><a href="breadcrumbs.html" class="slide-item"> مسیر سایت</a></li>
                                    <li><a href="badge.html" class="slide-item"> نشان‌ها / قرص‌ها</a></li>
                                </ul>
                            </div>
                            <div class="">
                                <ul>
                                    <li><a href="panels.html" class="slide-item"> پانل‌ها</a></li>
                                    <li><a href="thumbnails.html" class="slide-item"> تصاویر کوچک</a></li>
                                    <li><a href="offcanvas.html" class="slide-item">منوی جانبی</a></li>
                                    <li><a href="toast.html" class="slide-item"> تست</a></li>
                                    <li><a href="scrollspy.html" class="slide-item"> نشانگر پیمایش</a>
                                    </li>
                                    <li><a href="mediaobject.html" class="slide-item"> مدیا آبجکت</a></li>
                                </ul>
                            </div>
                            <div class="">
                                <ul>
                                    <li><a href="accordion.html" class="slide-item"> آکاردئون </a></li>
                                    <li><a href="tabs.html" class="slide-item"> برگه‌ها</a></li>
                                    <li><a href="modal.html" class="slide-item"> مودال</a></li>
                                    <li><a href="tooltipandpopover.html" class="slide-item"> تولتیپ و پاپ
                                            اور</a></li>
                                    <li><a href="progress.html" class="slide-item"> پیشرفت</a></li>
                                    <li><a href="carousel.html" class="slide-item"> اسلایدر</a></li>
                                    <li><a href="ribbons.html" class="slide-item"> ریبون ها</a></li>
                                </ul>
                            </div>
                        </div>
                    </ul>
                </li>
                <li>
                    <a class="side-menu__item has-link" href="landing-page.html" target="_blank"><i
                            class="side-menu__icon fe fe-zap"></i><span class="side-menu__label">لیندینگ
                            پیج</span><span
                            class="badge bg-green br-5 side-badge blink-text pb-1">جدید</span></a>
                </li>
                <li class="sub-category">
                    <h3>صفحات از قبل ساخته شده</h3>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-layers"></i><span
                            class="side-menu__label">صفحات</span><i
                            class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">صفحات</a></li>
                        <li><a href="profile.html" class="slide-item"> پروفایل</a></li>
                        <li><a href="editprofile.html" class="slide-item"> ویرایش پروفایل</a></li>
                        <li><a href="notify-list.html" class="slide-item"> فهرست اعلان‌ها</a></li>
                        <li><a href="email-compose.html" class="slide-item"> نوشتن ایمیل</a></li>
                        <li><a href="email-inbox.html" class="slide-item"> صندوق ورودی ایمیل</a></li>
                        <li><a href="email-read.html" class="slide-item"> محتوای ایمیل</a></li>
                        <li><a href="gallery.html" class="slide-item"> گللری</a></li>
                        <li class="sub-slide">
                            <a class="sub-side-menu__item" data-bs-toggle="sub-slide"
                               href="javascript:void(0)"><span class="sub-side-menu__label">فرم‌ها</span>
                                <i class="sub-angle fe fe-chevron-right"></i></a>
                            <ul class="sub-slide-menu">
                                <li><a href="form-elements.html" class="sub-slide-item"> عناصر فرم</a>
                                </li>
                                <li><a href="form-layouts.html" class="sub-slide-item"> طرح‌بندی فرم</a>
                                </li>
                                <li><a href="form-advanced.html" class="sub-slide-item"> فرم پیشرفته</a>
                                </li>
                                <li><a href="form-editor.html" class="sub-slide-item"> ویرایشگر فرم</a>
                                </li>
                                <li><a href="form-wizard.html" class="sub-slide-item"> فرم مرحله ای</a>
                                </li>
                                <li><a href="form-validation.html" class="sub-slide-item"> اعتبار سنجی
                                        فرم</a></li>
                                <li><a href="form-input-spinners.html" class="sub-slide-item"> اسپینرهای
                                        ورودی فرم</a></li>
                            </ul>
                        </li>
                        <li class="sub-slide">
                            <a class="sub-side-menu__item" data-bs-toggle="sub-slide"
                               href="javascript:void(0)"><span class="sub-side-menu__label">جدول</span>
                                <i class="sub-angle fe fe-chevron-right"></i></a>
                            <ul class="sub-slide-menu">
                                <li><a href="tables.html" class="sub-slide-item">جدول پیش‌فرض</a></li>
                                <li><a href="datatable.html" class="sub-slide-item"> جداول داده</a></li>
                                <li><a href="edit-table.html" class="sub-slide-item"> ویرایش جداول</a>
                                </li>
                                <li><a href="extension-tables.html" class="sub-slide-item"> جداول برنامه
                                        افزودنی</a></li>
                            </ul>
                        </li>
                        <li class="sub-slide">
                            <a class="sub-side-menu__item" data-bs-toggle="sub-slide"
                               href="javascript:void(0)"><span class="sub-side-menu__label">دیگر
                                    صفحات</span> <i class="sub-angle fe fe-chevron-right"></i></a>
                            <ul class="sub-slide-menu">
                                <li><a href="about.html" class="sub-slide-item"> درباره شرکت</a></li>
                                <li><a href="services.html" class="sub-slide-item"> خدمات</a></li>
                                <li><a href="faq.html" class="sub-slide-item"> سوالات متداول</a></li>
                                <li><a href="terms.html" class="sub-slide-item"> شرایط</a></li>
                                <li><a href="invoice.html" class="sub-slide-item"> فاکتور</a></li>
                                <li><a href="pricing.html" class="sub-slide-item"> جداول قیمت‌گذاری</a>
                                </li>
                                <li><a href="settings.html" class="sub-slide-item"> تنظیمات</a></li>
                                <li><a href="blog.html" class="sub-slide-item"> وبلاگ</a></li>
                                <li><a href="blog-details.html" class="sub-slide-item"> جزئیات وبلاگ</a>
                                </li>
                                <li><a href="blog-post.html" class="sub-slide-item"> پست وبلاگ</a></li>
                                <li><a href="empty.html" class="sub-slide-item"> صفحه خالی</a></li>
                                <li><a href="construction.html" class="sub-slide-item"> در حال ساخت</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="switcher-1.html" class="slide-item"> سوئیچر</a></li>
                    </ul>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-shopping-bag"></i><span
                            class="side-menu__label">تجارت الکترونیک</span><i
                            class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">تجارت الکترونیک</a>
                        </li>
                        <li><a href="shop.html" class="slide-item"> فروشگله</a></li>
                        <li><a href="shop-description.html" class="slide-item"> جزئیات محصول</a></li>
                        <li><a href="cart.html" class="slide-item"> سبد خرید</a></li>
                        <li><a href="add-product.html" class="slide-item"> افزودن محصول</a></li>
                        <li><a href="wishlist.html" class="slide-item"> فهرست علاقه مندی</a></li>
                        <li><a href="checkout.html" class="slide-item"> تسویه حساب</a></li>
                    </ul>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-folder"></i><span class="side-menu__label">مدیر
                            فایل</span><span class="badge bg-pink side-badge">4</span><i
                            class="angle fe fe-chevron-right hor-angle"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">مدیر فایل</a></li>
                        <li><a href="file-manager.html" class="slide-item"> مدیر فایل</a></li>
                        <li><a href="filemanager-list.html" class="slide-item"> فهرست مدیر فایل</a></li>
                        <li><a href="filemanager-details.html" class="slide-item"> جزئیات فایل</a></li>
                        <li><a href="file-attachments.html" class="slide-item"> فایل های پیوست </a></li>
                    </ul>
                </li>
                <li class="sub-category">
                    <h3>صفحات متفرقه</h3>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-users"></i><span class="side-menu__label">احراز
                            هویت</span><i class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">احراز هویت</a></li>
                        <li><a href="login.html" class="slide-item"> ورود به سیستم</a></li>
                        <li><a href="register.html" class="slide-item"> ثبت نام</a></li>
                        <li><a href="forgot-password.html" class="slide-item"> فراموشی رمز</a></li>
                        <li><a href="lockscreen.html" class="slide-item"> صفحه قفل</a></li>
                        <li class="sub-slide">
                            <a class="sub-side-menu__item" data-bs-toggle="sub-slide"
                               href="javascript:void(0)"><span class="sub-side-menu__label">صفحات
                                    خطا</span><i class="sub-angle fe fe-chevron-right"></i></a>
                            <ul class="sub-slide-menu">
                                <li><a href="400.html" class="sub-slide-item"> 400</a></li>
                                <li><a href="401.html" class="sub-slide-item"> 401</a></li>
                                <li><a href="403.html" class="sub-slide-item"> 403</a></li>
                                <li><a href="404.html" class="sub-slide-item"> 404</a></li>
                                <li><a href="500.html" class="sub-slide-item"> 500</a></li>
                                <li><a href="503.html" class="sub-slide-item"> 503</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fe fe-cpu"></i>
                        <span class="side-menu__label">موارد زیر منو</span><i
                            class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">موارد زیر منو</a></li>
                        <li><a href="javascript:void(0)" class="slide-item">زیرمنوی-1</a></li>
                        <li class="sub-slide">
                            <a class="sub-side-menu__item" data-bs-toggle="sub-slide"
                               href="javascript:void(0)"><span class="sub-side-menu__label">زیرمنوی
                                    2</span><i class="sub-angle fe fe-chevron-right"></i></a>
                            <ul class="sub-slide-menu">
                                <li><a class="sub-slide-item" href="javascript:void(0)">زیر منو-2.1</a>
                                </li>
                                <li><a class="sub-slide-item" href="javascript:void(0)">زیر منوی 2.2</a>
                                </li>
                                <li class="sub-slide2">
                                    <a class="sub-side-menu__item2" href="javascript:void(0)"
                                       data-bs-toggle="sub-slide2"><span
                                            class="sub-side-menu__label2">زیرمنوی-2.3</span><i
                                            class="sub-angle2 fe fe-chevron-right"></i></a>
                                    <ul class="sub-slide-menu2">
                                        <li><a href="javascript:void(0)"
                                               class="sub-slide-item2">زیرمنوی-2.3.1</a></li>
                                        <li><a href="javascript:void(0)"
                                               class="sub-slide-item2">زیرمنوی-2.3.2</a></li>
                                        <li><a href="javascript:void(0)" class="sub-slide-item2">زیر
                                                منو-2.3.3</a></li>
                                    </ul>
                                </li>
                                <li><a class="sub-slide-item" href="javascript:void(0)">زیرمنوی-2.4</a>
                                </li>
                                <li><a class="sub-slide-item" href="javascript:void(0)">زیر منو-2.5</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="sub-category">
                    <h3>عمومی</h3>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-map-pin"></i><span
                            class="side-menu__label">نقشه ها</span><i
                            class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">نقشه‌ها</a></li>
                        <li><a href="maps1.html" class="slide-item">نقشه‌های بروشور</a></li>
                        <li><a href="maps2.html" class="slide-item">Mapel Maps</a></li>
                        <li><a href="maps.html" class="slide-item">نقشه‌های برداری</a></li>
                    </ul>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-bar-chart-2"></i><span
                            class="side-menu__label">نمودار ها</span><span
                            class="badge bg-secondary side-badge">6</span><i
                            class="angle fe fe-chevron-right hor-angle"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">نمودارها</a></li>
                        <li><a href="chart-chartist.html" class="slide-item">Js نمودار</a></li>
                        <li><a href="chart-flot.html" class="slide-item">نمودار Flot</a></li>
                        <li><a href="chart-echart.html" class="slide-item"> نمودار ECharts</a></li>
                        <li><a href="chart-morris.html" class="slide-item"> نمودارهای Morris</a></li>
                        <li><a href="chart-nvd3.html" class="slide-item"> نمودارهای Nvd3</a></li>
                        <li><a href="charts.html" class="slide-item"> نمودارهای نواری C3</a></li>
                        <li><a href="chart-line.html" class="slide-item"> نمودارهای خطی C3</a></li>
                        <li><a href="chart-donut.html" class="slide-item"> نمودارهای دونات C3</a></li>
                        <li><a href="chart-pie.html" class="slide-item"> نمودارهای C3 Pie</a></li>
                    </ul>
                </li>
                <li class="slide">
                    <a class="side-menu__item" data-bs-toggle="slide" href="javascript:void(0)"><i
                            class="side-menu__icon fe fe-wind"></i><span class="side-menu__label">آیکن
                            ها</span><i class="angle fe fe-chevron-right"></i></a>
                    <ul class="slide-menu">
                        <li class="side-menu-label1"><a href="javascript:void(0)">آیکن ها</a></li>
                        <li><a href="icons.html" class="slide-item"> Font Awesome</a></li>
                        <li><a href="icons2.html" class="slide-item"> آیکن متریال</a></li>
                        <li><a href="icons3.html" class="slide-item"> آیکن خط ساده</a></li>
                        <li><a href="icons4.html" class="slide-item"> آیکن Feather</a></li>
                        <li><a href="icons5.html" class="slide-item"> نمادهای Ionic</a></li>
                        <li><a href="icons6.html" class="slide-item"> آیکن پرچم</a></li>
                        <li><a href="icons7.html" class="slide-item"> آیکن pe7</a></li>
                        <li><a href="icons8.html" class="slide-item"> آیکن Themify</a></li>
                        <li><a href="icons9.html" class="slide-item">آیکن Typicons</a></li>
                        <li><a href="icons10.html" class="slide-item">آیکن آب و هوا</a></li>
                        <li><a href="icons11.html" class="slide-item">آیکن بوت استرپ</a></li>
                    </ul>
                </li>
            </ul>
            <div class="slide-right" id="slide-right">
                <svg xmlns="http://www.w3.org/2000/svg"
                     fill="#7b8191" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z"/>
                </svg>
            </div>
        </div>
    </div>

</div>
