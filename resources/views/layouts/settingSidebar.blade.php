<div class="switcher-wrapper">
    <div class="demo_changer">
        <div class="form_holder sidebar-right1">
            <div class="row">
                <div class="predefined_styles">
                    
                    <div class="swichermainleft">
                        <h4>حالت چپ چین / راست چین</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="me-auto">حالت چپ چین</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch7"
                                            id="myonoffswitch23" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch23" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">حالت راست چین</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch7"
                                            id="myonoffswitch24" class="onoffswitch2-checkbox" checked>
                                        <label for="myonoffswitch24" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت روشن قالب</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="me-auto">قالب روشن</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch1"
                                            id="myonoffswitch1" class="onoffswitch2-checkbox" checked>
                                        <label for="myonoffswitch1" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex">
                                    <span class="me-auto">رنگ اصلی در روشن</span>
                                    <div class="">
                                        <input class="w-30p h-30 input-color-picker color-primary-light"
                                            value="#6c5ffc" id="colorID" oninput="changePrimaryColor()"
                                            type="color" data-id="bg-color" data-id1="bg-hover"
                                            data-id2="bg-border" data-id7="transparentcolor" name="lightPrimary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت تیره قالب</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">قالب تیره</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch1"
                                            id="myonoffswitch2" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch2" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">رنگ اصلی در تیره</span>
                                    <div class="">
                                        <input class="w-30p h-30 input-dark-color-picker color-primary-dark"
                                            value="#6c5ffc" id="darkPrimaryColorID" oninput="darkPrimaryColor()"
                                            type="color" data-id="bg-color" data-id1="bg-hover"
                                            data-id2="bg-border" data-id3="primary" data-id8="transparentcolor"
                                            name="darkPrimary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت شیشه ای</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">قالب شیشه ای</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch1"
                                            id="myonoffswitchTransparent" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitchTransparent" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">رنگ اصلی در حالت شیشه ای</span>
                                    <div class="">
                                        <input
                                            class="w-30p h-30 input-transparent-color-picker color-primary-transparent"
                                            value="#6c5ffc" id="transparentPrimaryColorID"
                                            oninput="transparentPrimaryColor()" type="color" data-id="bg-color"
                                            data-id1="bg-hover" data-id2="bg-border" data-id3="primary"
                                            data-id4="primary" data-id9="transparentcolor"
                                            name="tranparentPrimary">
                                    </div>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">پس زمینه حالت شیشه ای</span>
                                    <div class="">
                                        <input
                                            class="w-30p h-30 input-transparent-color-picker color-bg-transparent"
                                            value="#6c5ffc" id="transparentBgColorID"
                                            oninput="transparentBgColor()" type="color" data-id5="body"
                                            data-id6="theme" data-id9="transparentcolor"
                                            name="transparentBackground">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت پس زمینه شیشه ای</h4>
                        <div class="skin-body switch_section">
                            <div class="switch-toggle d-flex">
                                <span class="me-auto">تصویر زمینه اصلی </span>
                                <div class="">
                                    <input
                                        class="w-30p h-30 input-transparent-color-picker color-primary-transparent"
                                        value="#6c5ffc" id="transparentBgImgPrimaryColorID"
                                        oninput="transparentBgImgPrimaryColor()" type="color"
                                        data-id="bg-color" data-id1="bg-hover" data-id2="bg-border"
                                        data-id3="primary" data-id4="primary" data-id9="transparentcolor"
                                        name="tranparentPrimary">
                                </div>
                            </div>
                            <div class="switch-toggle d-flex mt-2">
                                <a class="bg-img1" href="javascript:void(0);" onclick="bgImage(this)"><img
                                        src="../assets/images/media/bg-img1.jpg" alt="Bg-Image"
                                        id="bgimage1"></a>
                                <a class="bg-img2" href="javascript:void(0);" onclick="bgImage(this)"><img
                                        src="../assets/images/media/bg-img2.jpg" alt="Bg-Image"
                                        id="bgimage2"></a>
                                <a class="bg-img3" href="javascript:void(0);" onclick="bgImage(this)"><img
                                        src="../assets/images/media/bg-img3.jpg" alt="Bg-Image"
                                        id="bgimage3"></a>
                                <a class="bg-img4" href="javascript:void(0);" onclick="bgImage(this)"><img
                                        src="../assets/images/media/bg-img4.jpg" alt="Bg-Image"
                                        id="bgimage4"></a>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت منو</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle lightMenu d-flex">
                                    <span class="me-auto">منو روشن</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch2"
                                            id="myonoffswitch3" class="onoffswitch2-checkbox" checked>
                                        <label for="myonoffswitch3" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle colorMenu d-flex mt-2">
                                    <span class="me-auto">رنگ منو</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch2"
                                            id="myonoffswitch4" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch4" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle darkMenu d-flex mt-2">
                                    <span class="me-auto">منو تیره</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch2"
                                            id="myonoffswitch5" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch5" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle gradientMenu d-flex mt-2">
                                    <span class="me-auto">منو گرادیانت</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch2"
                                            id="myonoffswitch19" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch19" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت سربرگ</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle lightHeader d-flex">
                                    <span class="me-auto">سربرگ روشن</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch3"
                                            id="myonoffswitch6" class="onoffswitch2-checkbox" checked>
                                        <label for="myonoffswitch6" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle  colorHeader d-flex mt-2">
                                    <span class="me-auto">سربرگ رنگی</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch3"
                                            id="myonoffswitch7" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch7" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle darkHeader d-flex mt-2">
                                    <span class="me-auto">سربرگ تیره</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch3"
                                            id="myonoffswitch8" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch8" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle darkHeader d-flex mt-2">
                                    <span class="me-auto">سربرگ گرادیانت</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch3"
                                            id="myonoffswitch20" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch20" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>حالت های عرض محتوا</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="me-auto">تمام عرض</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch4"
                                            id="myonoffswitch9" class="onoffswitch2-checkbox" checked>
                                        <label for="myonoffswitch9" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">جعبه ای</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch4"
                                            id="myonoffswitch10" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch10" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>موقعیت های چیدمان</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="me-auto">ثابت</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch5"
                                            id="myonoffswitch11" class="onoffswitch2-checkbox" checked>
                                        <label for="myonoffswitch11" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">قابل اسکرول</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch5"
                                            id="myonoffswitch12" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch12" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft leftmenu-styles">
                        <h4>حالت های منوی کناری</h4>
                        <div class="skin-body">
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="me-auto">منوی پیشفرض</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch6"
                                            id="myonoffswitch13" class="onoffswitch2-checkbox default-menu"
                                            checked>
                                        <label for="myonoffswitch13" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">آیکن با متن</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch6"
                                            id="myonoffswitch14" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch14" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">فقط آیکن</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch6"
                                            id="myonoffswitch15" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch15" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">حالت بسته</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch6"
                                            id="myonoffswitch16" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch16" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">نمایش زیرمنو با هاور</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch6"
                                            id="myonoffswitch17" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch17" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                                <div class="switch-toggle d-flex mt-2">
                                    <span class="me-auto">نمایش زیرمنو با هاور 2</span>
                                    <p class="onoffswitch2"><input type="radio" name="onoffswitch6"
                                            id="myonoffswitch18" class="onoffswitch2-checkbox">
                                        <label for="myonoffswitch18" class="onoffswitch2-label"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>فونت قالب </h4>
                        <div class="skin-body">
                            <label for="font-changer-en" class="text-start d-block px-3"> ( اعداد انگلیسی )
                            </label>
                            <select name="font-changer" id="font-changer-en" class="form-control form-select">
                                <option value="iran-yekan">ایران یکان</option>
                                <option value="iran-sans">ایران سنس</option>
                                <option value="aviny">آوینی</option>
                                <option value="azarmehr">آذرمهر</option>
                                <option value="damavand">دماوند</option>
                                <option value="dastnevis">دست نویس</option>
                                <option value="dubai">دوبی</option>
                                <option value="estedad">استعداد</option>
                                <option value="helvetica-neue">هلوتیکا</option>
                                <option value="lalezar">لاله زار</option>
                                <option value="mikhak">میخک</option>
                                <option value="myriad">میریاد</option>
                                <option value="noora">نورا</option>
                                <option value="palatino-sans">پالاتینو</option>
                                <option value="pinar">پینار</option>
                                <option value="dana">دانا</option>
                                <option value="sahel">ساحل</option>
                                <option value="shabnam">شبنم</option>
                                <option value="vanda">واندا</option>
                                <option value="vazir">وزیر</option>
                            </select>
                        </div>
                        <div class="skin-body mb-3">
                            <label for="font-changer" class="text-start d-block px-3"> ( اعداد فارسی ) </label>
                            <select name="font-changer" id="font-changer" class="form-control form-select">
                                <option value="iran-yekan">ایران یکان</option>
                                <option value="iran-sans">ایران سنس</option>
                                <option value="aviny">آوینی</option>
                                <option value="azarmehr">آذرمهر</option>
                                <option value="damavand">دماوند</option>
                                <option value="dastnevis">دست نویس</option>
                                <option value="dubai">دوبی</option>
                                <option value="estedad">استعداد</option>
                                <option value="helvetica-neue">هلوتیکا</option>
                                <option value="lalezar">لاله زار</option>
                                <option value="mikhak">میخک</option>
                                <option value="myriad">میریاد</option>
                                <option value="noora">نورا</option>
                                <option value="palatino-sans">پالاتینو</option>
                                <option value="pinar">پینار</option>
                                <option value="dana">دانا</option>
                                <option value="sahel">ساحل</option>
                                <option value="shabnam">شبنم</option>
                                <option value="vanda">واندا</option>
                                <option value="vazir">وزیر</option>
                            </select>
                        </div>
                    </div>
                    <div class="swichermainleft">
                        <h4>بازنشانی همه تنظیمات</h4>
                        <div class="skin-body">
                            <div class="switch_section my-4">
                                <button class="btn btn-danger btn-block"
                                    onclick="localStorage.clear();
                                        document.querySelector('html').style = '';
                                        names() ;
                                        resetData() ;"
                                    type="button">بازنشانی
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>