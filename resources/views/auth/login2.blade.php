<!doctype html>
<html lang="fa-IR" dir="rtl">

<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sash – الگوی مدیریت و داشبورد بوت استرپ 5">
    <meta name="author" content="limoostudio">
    <meta name="keywords"
        content=" پنل پشتیبانان اپلیکیشن بنیانگذار ">

    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/brand/favicon.ico" />

    <title>پنل پشتیبانان اپلیکیشن بنیانگذار</title>

    <link id="style" href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../assets/css/style.css" rel="stylesheet" />
    <link href="../assets/css/dark-style.css" rel="stylesheet" />
    <link href="../assets/css/transparent-style.css" rel="stylesheet">
    <link href="../assets/css/skin-modes.css" rel="stylesheet" />

    <link href="../assets/css/icons.css" rel="stylesheet" />

    <link id="theme" rel="stylesheet" type="text/css" media="all" href="../assets/colors/color1.css" />
    <link href="../assets/switcher/css/switcher.css" rel="stylesheet" />
    <link href="../assets/switcher/demo.css" rel="stylesheet" />
    <link rel="stylesheet" id="fonts" href="../assets/fonts/styles-fa-num/iran-yekan.css">
    <link href="../assets/css/rtl.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini rtl login-img">

    <div class="">

        <div id="global-loader">
            <img src="../assets/images/loader.svg" class="loader-img" alt="Loader">
        </div>


        <div class="page">
            <div class="">

                <div class="col col-login mx-auto mt-7">
                    <div class="text-center">
                        <img src="../assets/images/brand/logo-white.png" class="header-brand-img" alt="">
                    </div>
                </div>
                <div class="container-login100">
                    <div class="wrap-login100 p-6">
                        <form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
                            @csrf
                            <span class="login100-form-title pb-5">
                                ورود به پنل کاربری
                            </span>
                            <div class="panel panel-primary">
                                <div class="panel-body tabs-menu-body p-0 pt-5">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab5">

                                            <!-- Email Address -->
                                            <div>
                                                <x-input-error :messages="$errors->get('username')" class="mt-2" />
                                            </div>

                                            <!-- Password -->
                                            <div class="mt-4">
                                                <x-input-error :messages="$errors->get('password')" class="mt-2" />
                                            </div>

                                            <div class="wrap-input100 validate-input input-group">
                                                <a href="javascript:void(0)"
                                                    class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-email text-muted" aria-hidden="true"></i>
                                                </a>
                                                <input class="input100 border-start-0 form-control ms-0" type="string"
                                                    name="username" value="{{old('username')}}" required autofocus
                                                    autocomplete="username" placeholder="نام کاربری">
                                            </div>

                                            <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                                <a href="javascript:void(0)"
                                                    class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                                </a>
                                                <input class="input100 border-start-0 form-control ms-0"
                                                name="password" required autocomplete="password"
                                                    type="password" placeholder="رمز عبور">
                                            </div>

                                            <!-- Remember Me -->
                                            <div class="block mt-4">
                                                <label for="remember_me" class="inline-flex items-center">
                                                    <input id="remember_me" type="checkbox"
                                                        class="rounded dark:bg-gray-900 border-gray-300 dark:border-gray-700 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800"
                                                        name="remember">
                                                    <span
                                                        class="ml-2 text-sm text-gray-600 dark:text-gray-400">{{ __('مرا به خاطر بسپار') }}</span>
                                                </label>
                                            </div>


                                            <div class="container-login100-form-btn">
                                                <button type="submit" class="login100-form-btn btn-primary">
                                                    ورود
                                                </a>
                                            </div>
                                            <div class="text-end pt-4">
                                                <p class="mb-0"><a href="forgot-password.html"
                                                        class="text-primary ms-1">فراموشی رمز؟</a></p>
                                            </div>

                                            <div class="text-center pt-3">
                                                <p class="text-dark mb-0">عضو نیستید؟<a href="register.html"
                                                        class="text-primary ms-1">عضو شوید</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <script src="../assets/js/jquery.min.js"></script>

    <script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="../assets/js/show-password.min.js"></script>

    <script src="../assets/js/generate-otp.js"></script>

    <script src="../assets/plugins/p-scroll/perfect-scrollbar.js"></script>

    <script src="../assets/js/themeColors.js"></script>

    <script src="../assets/js/custom.js"></script>
    <script src="../assets/js/custom1.js"></script>

    <script src="../assets/switcher/js/switcher.js"></script>
</body>

</html>
